package com.example.piotr.podstawyandroida;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Detale extends Activity {
    public static final String RESULT = "RESULT";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    TextView dateTextView;
    EditText detailIdTextView;
    Wydarzenie wydarzenie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detale);
        detailIdTextView = (EditText) findViewById(R.id.detail_id);
        dateTextView = (TextView) findViewById(R.id.data_wydarzenia);
        wydarzenie = (Wydarzenie) getIntent().getSerializableExtra("WARTOSC");
        Button chooseDateButton = (Button) findViewById(R.id.data_wydarzenia_button);
        if(wydarzenie != null) {
            detailIdTextView.setText(wydarzenie.getName());
        }

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(wydarzenie.getDataWydarzenia());

        setTextViewTime(wydarzenie.getDataWydarzenia());
        chooseDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Detale.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Date newDate = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
                        wydarzenie.setDataWydarzenia(newDate);
                        setTextViewTime(newDate);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .show();
            }
        });
    }

    private void setTextViewTime(Date data) {
        dateTextView.setText(simpleDateFormat.format(data));
    }

    public void clicked(View v) {
        String text = detailIdTextView.getText().toString();
        wydarzenie.setName(text);
        Intent intent = new Intent();
        intent.putExtra(RESULT, wydarzenie);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
