package com.example.piotr.podstawyandroida;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends ActionBarActivity {
    private static final String LISTA_ELEMENTOW = "LISTA_ELEMENTOW";
    public static final int EDYTUJ_ITEM = 5;
    private NaszAdapter wydarzenia;
    List<Wydarzenie> listaElementow = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button add = (Button) findViewById(R.id.button);
        final ListView listView = (ListView) findViewById(R.id.wrapper);
        final EditText editText = (EditText) findViewById(R.id.editText);
        if(savedInstanceState != null) {
            listaElementow = (List<Wydarzenie>) savedInstanceState.getSerializable(LISTA_ELEMENTOW);
        }
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wartosc = editText.getText().toString();
                wydarzenia.add(new Wydarzenie(wartosc));
                editText.setText("");
            }
        });
        wydarzenia = new NaszAdapter();
        listView.setAdapter(wydarzenia);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Wydarzenie item = (Wydarzenie) wydarzenia.getItem(position);
                Intent intent = new Intent(MainActivity.this, Detale.class);
                intent.putExtra("WARTOSC", item);
                startActivityForResult(intent, EDYTUJ_ITEM);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                String item = (String) wydarzenia.getItem(position);
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Czy na pewno chcesz usunac " + item)
                        .setNegativeButton("Nie", null)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                wydarzenia.remove(position);
                            }
                        })
                        .show();

                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(LISTA_ELEMENTOW, (Serializable) listaElementow);
        super.onSaveInstanceState(outState);
    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == EDYTUJ_ITEM && resultCode == Activity.RESULT_OK) {
            Wydarzenie wydarzenie = (Wydarzenie) data.getSerializableExtra(Detale.RESULT);
            Toast.makeText(MainActivity.this, wydarzenie.getName() + " data: " + simpleDateFormat.format(wydarzenie.getDataWydarzenia()), Toast.LENGTH_SHORT).show();

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class NaszAdapter extends BaseAdapter {
        private final LayoutInflater inflater;

        public NaszAdapter() {
            this.inflater = LayoutInflater.from(MainActivity.this);
        }

        @Override
        public int getCount() {
            return listaElementow.size();
        }

        @Override
        public Object getItem(int position) {
            return listaElementow.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = inflater.inflate(R.layout.element_list, null);
            }
            Wydarzenie wydarzenie = listaElementow.get(position);
            TextView textView = (TextView) convertView.findViewById(R.id.element_list_text);
            textView.setText(wydarzenie.getName());
            return convertView;
        }

        public void add(Wydarzenie wydarzenie) {
            listaElementow.add(wydarzenie);
            notifyDataSetChanged();
        }

        public void remove(int position) {
            listaElementow.remove(position);
            notifyDataSetChanged();
        }
    }
}
