package com.example.piotr.podstawyandroida;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by piotr on 02.11.15.
 */
public class Wydarzenie implements Serializable {
    private String name;
    private Date dataWydarzenia = new Date();

    public Wydarzenie(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDataWydarzenia() {
        return dataWydarzenia;
    }

    public void setDataWydarzenia(Date dataWydarzenia) {
        this.dataWydarzenia = dataWydarzenia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wydarzenie that = (Wydarzenie) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return !(dataWydarzenia != null ? !dataWydarzenia.equals(that.dataWydarzenia) : that.dataWydarzenia != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (dataWydarzenia != null ? dataWydarzenia.hashCode() : 0);
        return result;
    }
}
